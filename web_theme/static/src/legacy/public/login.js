/** @odoo-module */

import dom from '@web/legacy/js/core/dom';
import { session } from "@web/session";
import publicWidget from '@web/legacy/js/public/public_widget';

publicWidget.registry.RainBowLogin = publicWidget.Widget.extend({
    selector: '.oe_login_form',
    events: {

    },

    start: function () {

        // console.log("RainBowLogin start-----", session["hide_login_as_buttons"]);
        if (session["hide_login_as_buttons"]) {
            // dom.query('.ou-login-buttons').remove();
            const login_as_buttons = this.el.querySelector('.ou-login-buttons').parentNode;
            // console.log("login_as_buttons-----", login_as_buttons);
            login_as_buttons.remove();
        }
    }

});
