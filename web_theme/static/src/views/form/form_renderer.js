/** @odoo-module */

import { SIZES } from "@web/core/ui/ui_service";
import { useBus, useService } from "@web/core/utils/hooks";
import { patch } from "@web/core/utils/patch";
import { session } from "@web/session";
import { FormRenderer } from "@web/views/form/form_renderer";
import { onMounted, useRef, useEffect } from "@odoo/owl";

patch(FormRenderer.prototype, {
    setup() {
        super.setup(...arguments);
        this.ui = useService("ui");
        const self = this;

        const offset = 300;
        const rootRef = useRef("compiled_view_root");
        onMounted(() => {
            if (session["theme"]["display_scroll_top_button"]) {
                this.scrollTopButton = $(rootRef.el).closest(".o_action_manager").find('.o_scroll_top')[0];
            }
        })

        const onClickScrollToTop = (ev) => {
            let scrollTopButton = ev.target;
            if (scrollTopButton.tagName == 'span') {
                scrollTopButton = scrollTopButton.parentNode;
            } else if (scrollTopButton.tagName == 'svg') {
                scrollTopButton = scrollTopButton.parentNode.parentNode;
            } else if (scrollTopButton.tagName == 'path') {
                scrollTopButton = scrollTopButton.parentNode.parentNode.parentNode;
            } else if (scrollTopButton.tagName == 'rect') {
                scrollTopButton = scrollTopButton.parentNode.parentNode.parentNode;
            }

            const duration = 500; //时间
            $(this.scrollFormEl).animate(
                {
                    scrollTop: 0
                },
                duration
            );
            return false;
        }

        const onScrollFormEl = (ev) => {
            if (ev.currentTarget.scrollTop > offset) {
                this.scrollTopButton.classList.remove('o_hidden');
                this.scrollTopButton.addEventListener("click", onClickScrollToTop);
            } else {
                this.scrollTopButton.classList.add('o_hidden');
                this.scrollTopButton.removeEventListener("click", onClickScrollToTop);
            }
        }


        useEffect((FormEl) => {
            if (!session["theme"]["display_scroll_top_button"]) {
                return;
            }
            if (!FormEl) {
                return;
            }

            const { size } = this.ui;
            if (size <= SIZES.XS) {
                self.scrollFormEl = $(FormEl).find(".o_form_sheet_bg")[0];
            } else if (!this.env.inDialog && size === SIZES.XXL) {
                self.scrollFormEl = $(FormEl).find(".o_form_sheet_bg")[0];
            } else {
                self.scrollFormEl = $(FormEl).closest(".o_content")[0];
            }

            if (!self.scrollFormEl) {
                return;
            }
            self.scrollFormEl.addEventListener('scroll', onScrollFormEl);
        }, () => [rootRef.el])
    },
})