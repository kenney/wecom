/** @odoo-module */

import { patch } from "@web/core/utils/patch";
import { session } from "@web/session";
import { ListRenderer } from "@web/views/list/list_renderer";
import { onMounted, useEffect } from "@odoo/owl";

patch(ListRenderer.prototype, {
    setup() {
        super.setup(...arguments);
        const offset = 300;

        onMounted(() => {
            if (session["theme"]["display_scroll_top_button"]) {
                this.scrollTopButton = $(this.rootRef.el).closest(".o_action_manager").find('.o_scroll_top')[0];
            }
        })

        const onClickScrollToTop = (ev) => {
            let scrollTopButton = ev.target;
            if (scrollTopButton.tagName == 'span') {
                scrollTopButton = scrollTopButton.parentNode;
            } else if (scrollTopButton.tagName == 'svg') {
                scrollTopButton = scrollTopButton.parentNode.parentNode;
            } else if (scrollTopButton.tagName == 'path') {
                scrollTopButton = scrollTopButton.parentNode.parentNode.parentNode;
            } else if (scrollTopButton.tagName == 'rect') {
                scrollTopButton = scrollTopButton.parentNode.parentNode.parentNode;
            }

            const duration = 500; //时间
            $(this.rootRef.el).animate(
                {
                    scrollTop: 0
                },
                duration
            );
            return false;
        }

        const onScrollListEl = (ev) => {
            if (ev.currentTarget.scrollTop > offset) {
                this.scrollTopButton.classList.remove('o_hidden');
                this.scrollTopButton.addEventListener("click", onClickScrollToTop);
            } else {
                this.scrollTopButton.classList.add('o_hidden');
                this.scrollTopButton.removeEventListener("click", onClickScrollToTop);
            }
        }

        useEffect((listEl) => {
            if (!session["theme"]["display_scroll_top_button"]) {
                return;
            }
            if (!listEl) {
                return;
            }
            listEl.addEventListener('scroll', onScrollListEl);
        }, () => [this.rootRef.el])
    },
})