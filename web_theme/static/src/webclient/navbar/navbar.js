/** @odoo-module **/

import { NavBar } from "@web/webclient/navbar/navbar";
import { session } from "@web/session";
import { useService, useBus } from "@web/core/utils/hooks";
import { _t } from "@web/core/l10n/translation";
import { useEffect, useRef, useState } from "@odoo/owl";

export class WebThemeNavBar extends NavBar {
    setup() {
        super.setup();
        this.dm = useService("drawer_menu");
        this.menuAppsRef = useRef("menuApps");
        this.navRef = useRef("nav");
        this._busToggledCallback = () => this._updateMenuAppsIcon();
        useBus(this.env.bus, "drawer-menu:TOGGLED", this._busToggledCallback);
        useEffect(() => this._updateMenuAppsIcon());

        this.state = useState({
            theme: session["theme"],
        });
    }
    get hasBackgroundAction() {
        return this.dm.hasBackgroundAction;
    }
    get isInApp() {
        return !this.dm.hasDrawerMenu;
    }
    _updateMenuAppsIcon() {
        if (this.state.theme.menu_layout_mode === 3) {
            const menuAppsEl = this.menuAppsRef.el;
            menuAppsEl.classList.toggle("o_hidden", !this.isInApp && !this.hasBackgroundAction);
            menuAppsEl.classList.toggle(
                "o_menu_toggle_back",
                !this.isInApp && this.hasBackgroundAction
            );
            const title =
                !this.isInApp && this.hasBackgroundAction ? _t("Previous view") : _t("Drawer menu");
            menuAppsEl.title = title;
            menuAppsEl.ariaLabel = title;

            const menuBrand = this.navRef.el.querySelector(".o_menu_brand");
            if (menuBrand) {
                menuBrand.classList.toggle("o_hidden", !this.isInApp);
            }

            const menuBrandIcon = this.navRef.el.querySelector(".o_menu_brand_icon");
            if (menuBrandIcon) {
                menuBrandIcon.classList.toggle("o_hidden", !this.isInApp);
            }

            const appSubMenus = this.appSubMenus.el;
            if (appSubMenus) {
                appSubMenus.classList.toggle("o_hidden", !this.isInApp);
            }
        }
    }
}
WebThemeNavBar.template = "web_theme.WebThemeNavBar";
