/** @odoo-module **/

import { registry } from "@web/core/registry";
import { session } from "@web/session";
import { Mutex } from "@web/core/utils/concurrency";
import { useService } from "@web/core/utils/hooks";
import { computeAppsAndMenuItems, reorderApps } from "@web/webclient/menus/menu_helpers";
import {
    ControllerNotFoundError,
    standardActionServiceProps,
} from "@web/webclient/actions/action_service";
import { DrawerMenu } from "./drawer_menu";

import { Component, onMounted, onWillUnmount, xml, useState } from "@odoo/owl";

export const drawerMenuService = {
    dependencies: ["action", "router", "user"],
    start(env, { user }) {
        let hasDrawerMenu = false; // true iff the DrawerMenu is currently displayed
        let hasBackgroundAction = false; // true iff there is an action behind the DrawerMenu
        const mutex = new Mutex(); // used to protect against concurrent toggling requests
        class DrawerMenuAction extends Component {
            setup() {
                this.router = useService("router");
                this.menus = useService("menu");
                const user = useService("user");
                const drawermenuConfig = JSON.parse(user.settings?.drawermenu_config || "null");
                const apps = computeAppsAndMenuItems(this.menus.getMenuAsTree("root")).apps;
                if (drawermenuConfig) {
                    reorderApps(apps, drawermenuConfig);
                }
                this.drawerMenuProps = {
                    apps: apps,
                };

                this.state = useState({
                    theme: session["theme"],
                });

                onMounted(() => this.onMounted());
                onWillUnmount(this.onWillUnmount);
            }
            async onMounted() {
                const { breadcrumbs } = this.env.config;
                hasDrawerMenu = true;
                hasBackgroundAction = breadcrumbs.length > 0;
                this.env.bus.trigger("drawer-menu:TOGGLED");
            }
            onWillUnmount() {
                hasDrawerMenu = false;
                hasBackgroundAction = false;
                this.env.bus.trigger("drawer-menu:TOGGLED");
            }
        }
        DrawerMenuAction.components = { DrawerMenu };
        DrawerMenuAction.target = "current";
        DrawerMenuAction.props = { ...standardActionServiceProps };
        DrawerMenuAction.template = xml`<DrawerMenu t-props="drawerMenuProps"/>`;

        registry.category("actions").add("menu", DrawerMenuAction);

        env.bus.addEventListener("drawer-menu:TOGGLED", () => {
            document.body.classList.toggle("o_drawer_menu_background", hasDrawerMenu);
        });

        return {
            get hasDrawerMenu() {
                return hasDrawerMenu;
            },
            get hasBackgroundAction() {
                return hasBackgroundAction;
            },
            async toggle(show) {

                return mutex.exec(async () => {
                    show = show === undefined ? !hasDrawerMenu : Boolean(show);
                    if (show !== hasDrawerMenu) {
                        if (show) {
                            await env.services.action.doAction("menu");
                        } else {
                            try {
                                await env.services.action.restore();
                            } catch (err) {
                                if (!(err instanceof ControllerNotFoundError)) {
                                    throw err;
                                }
                            }
                        }
                    }
                    // hack: wait for a tick to ensure that the url has been updated before
                    // switching again
                    return new Promise((r) => setTimeout(r));
                });
            },
        };
    },
};

registry.category("services").add("drawer_menu", drawerMenuService);
