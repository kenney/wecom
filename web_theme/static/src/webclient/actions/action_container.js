/** @odoo-module **/

import { session } from "@web/session";
import { ActionContainer } from '@web/webclient/actions/action_container';
import { onMounted, useState, useRef, useEffect } from "@odoo/owl";


export class WebThemeActionContainer extends ActionContainer {
    setup() {
        super.setup();

        this.state = useState({
            ...this.state,
            theme: session["theme"]
        });
        this.ActionContainerEl = useRef('action_manager');
    }


}
WebThemeActionContainer.components = {
    ...ActionContainer.components,
};
WebThemeActionContainer.template = 'web_theme.ActionContainer';

