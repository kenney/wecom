/** @odoo-module **/

import { useDebounced } from "@web/core/utils/timing";
import { browser } from "@web/core/browser/browser";
import { useService } from "@web/core/utils/hooks";
import { _lt, _t } from "@web/core/l10n/translation";
import { session } from "@web/session";
const {
    Component,
    onMounted,
    onRendered,
    onWillRender,
    onWillDestroy,
    onWillUnmount,
    onPatched,
    onWillUpdateProps,
    useExternalListener,
    useEffect,
    useState,
    useRef
} = owl;

/**
 * SideNav menu
 * 该组件处理不同可用应用程序和菜单之间的显示和导航。
 *
 *! 组件示例图：
 **┌ <div class="accordion accordion-flush" id="menu-accordion-1"> ---------------------------┐
 **│ ┌ <div class="accordion-item" id="menu-item-1"> ---------------------------------------┐ │
 **│ │ ┌ <h2 class="accordion-header"  id="menu-header-1"> -------------------------------┐ │ │
 **│ │ │ ┌ <a class="accordion-button collapsed" id="menu-link-1" href="#"> ------------┐ │ │ │
 **| | | |   data-bs-toggle="collapse"                                                  | | | |
 **| | | |   aria-expanded="false"                                                      │ │ │ │
 **| | | |   data-bs-target="#menu-collapse-1"                                          │ │ │ │
 **| | | |   aria-controls="menu-collapse-1"                                            │ │ │ │
 **| | | | >                                                                            │ │ │ │
 **| │ │ │ ┌ <span class="menu-icon"> ------------------------------------------------┐ │ │ │ │
 **| │ │ │ │  <i class="fa fa-home"></i>                                              │ │ │ │ │
 **| │ │ │ └ </span> -----------------------------------------------------------------┘ │ │ │ │
 **| │ │ │ - <span class="menu-lable"/>                                                 │ │ │ │
 **| │ │ └ <a/> ------------------------------------------------------------------------┘ │ │ │
 **| │ └ </h2> ---------------------------------------------------------------------------┘ │ │
 **| │ ┌ <div class="accordion-collapse collapse" id="menu-collapse-1"  折叠的选项 --------┐ │ │
 **| | |     aria-labelledby="menu-header-1"                                              │ │ │
 **| | |     data-bs-parent="#menu-accordion-1"                                           │ │ │
 **| │ │ >                                                                                │ │ │
 **| │ │ ┌ <div class="accordion-body" id="menu-body-1> --------------------------------┐ │ │ │
 **| │ │ │ - [Next level menu]  从<div class="accordion accordion-flush">开始循环        │ │ │ │
 **| │ │ └ </div>-----------------------------------------------------------------------┘ │ │ │
 **| │ └ </div>---------------------------------------------------------------------------┘ │ │
 **| └ </div> ------------------------------------------------------------------------------┘ │
 **└ </div>-----------------------------------------------------------------------------------┘

 *! 注意:
 1. ".collapse"类用于指定一个折叠元素 (示例中的 <div>); 点击按钮后会在隐藏与显示之间切换。
 2. 控制内容的隐藏与显示，需要在 <a> 或 <button> 元素上添加 data-bs-toggle="collapse" 属性。 data-bs-target="#id" 属性是对应折叠的内容 (<div id="menu-collapse-1">)。
 3. <a> 元素上你可以使用 href 属性来代替 data-bs-target 属性.
 4. 默认情况下折叠的内容是隐藏的，你可以添加 ".show" 类让内容默认显示:
 5. 使用 data-bs-parent 属性来确保所有的折叠元素在指定的父元素下，这样就能实现在一个折叠选项显示时其他选项就隐藏。

 */

export class SidebarMenu extends Component {
    setup() {
        super.setup();
        this.SidebarMenuRef = useRef("sidebar_menu");
        this.menuService = useService('menu');
        this.actionService = useService('action');
        this.router = useService('router');
        this.menuData = this.menuService.getMenuAsTree('root');
        // this.apps = this.menuService.getApps();
        this.state = useState({
            theme: session["theme"]
        });
        // console.log("--------------", this.menuService.getApps())
        // console.log("--------------", session.user_companies)
        // console.log("--------------", session.user_companies.current_company)
        this.current_cid = session.user_companies.current_company;
        this.state.sidebarMinimize = false;

        if (this.state.theme.sidebar_default_minimized) {
            this.state.sidebarMinimize = true;
        }


        this.currentMenuId = Number(this.router.current.hash.menu_id || 0);
        if (!this.currentMenuId) {
            this.currentMenuId = this.menuData.children[0];
        }
        this.currentAppId = this.menuService.getMenu(this.currentMenuId).appID;


        onMounted(() => {
            this.onMounted();
        });
    }

    onMounted() {
        // 定义挂载组件时应该执行的代码的钩子
        this.body = document.body;

        this.el = this.SidebarMenuRef.el;
        this.$el = $(this.el);

        let menuId = Number(this.router.current.hash.menu_id || 0);
        const currentController = this.actionService.currentController;
        let actionId = currentController && currentController.action.id;
        if (menuId === 0 && !actionId) {
            //TODO
        }

        this.initializeMenu();
    }

    //-----------------------------------------------
    // Handlers
    //-----------------------------------------------

    initializeMenu() {
        // 初始化菜单
        if (this.currentMenuId) {
            let $menu = this.$el.find('a#menu-link-' + this.currentMenuId);
            $menu.addClass('active');
            // console.log("初始化菜单", this.state.sidebarMinimize)
            if (!this.state.sidebarMinimize) {
                $menu.parents('.collapse').addClass('show');
            }

            // 'active' 菜单滚动到可视区域
            this.accordion_menu = this.$el.find("#o_sidebar_menu_accordion")
            // if ($menu.offset().top > (this.accordion_menu.height() / 2)) {
            //     this.accordion_menu.animate({ scrollTop: $menu.offset().top - 100 }, 500);
            // }
        }
    }

    allowLoadAppSubMenus() {

        if (this.state.theme.sidebar_default_minimized && !this.state.theme.sidebar_hover_maximize) {
            return false;
        }
        else if (this.state.theme.main_submenu_position === 1) {
            return false;
        } else {
            return true;
        }
    }

    currentAppSections(appid) {
        return (
            this.menuService.getMenuAsTree(appid).childrenTree || []
        );
    }

    currentMenuSections(menuid) {
        return (
            this.menuService.getMenuAsTree(menuid).childrenTree || []
        );
    }

    //-----------------------------------------------
    // Private
    //-----------------------------------------------

    _expandOrCollapseSideMenu(target, state) {
        if (!this.state.sidebarMinimize || !this.state.theme.sidebar_hover_maximize) {
            return
        }
        // console.log(target, state);
        if (state) {
            // 滑入侧边栏时，展开侧边栏
            this.el.classList.add("sidebar-maximize");
            let $menu = this.$el.find('a#menu-link-' + this.currentMenuId);
            $menu.parents('.collapse').addClass('show');
        } else {
            // 滑出侧边栏时，收起侧边栏
            this.el.classList.remove("sidebar-maximize");
            this.el.querySelectorAll(".accordion-collapse").forEach((item) => {
                item.classList.remove('show');
            })
        }
    }

    _showToggleSideMenuButton(target) {
        // console.log(target);
        if (!this.state.theme.sidebar_show_minimize_button) {
            return;
        }
        const logoEl = target.closest(".o_sidebar_menu_brand_logo");
        this.el.querySelector("button.o_sidebar_menu_toggler").classList.remove("o_hidden");
        logoEl.classList.add("o_hidden");
    }

    _hideToggleSideMenuButton(target) {
        if (this.state.sidebarMinimize) {
            const logoEl = this.el.querySelector(".o_sidebar_menu_brand_logo");
            target.classList.add("o_hidden");
            logoEl.classList.remove("o_hidden");
        }
    }

    _toggleSideMenu(target, minimize) {
        this.state.sidebarMinimize = !minimize;
        if (this.state.sidebarMinimize) {
            this.body.setAttribute('data-sidebar-minimize', 'on');
            this.$el.find('.accordion-collapse.show').removeClass('show');
        } else {
            this.body.setAttribute('data-sidebar-minimize', 'off');
            const logoEl = this.el.querySelector(".o_sidebar_menu_brand_logo");
            logoEl.classList.remove("o_hidden");
            this.initializeMenu();
        }
    }

    _openMenu(ev, menu) {
        // let $menu = $(ev.currentTarget);
        // console.log("点击菜单", $(ev.currentTarget), menu);

        // 移除所有的 'active'
        let allMenu = this.SidebarMenuRef.el.querySelectorAll('.accordion-button');
        allMenu.forEach(m => {
            m.classList.remove('active')
        });
        // $menu.addClass('active');

        if (this.env.isSmall) {
            // 手机端，点击菜单时，隐藏
            const close_btn = this.SidebarMenuRef.el.querySelector('#o_sidenav_mobile_close');
            close_btn.click();
        }

        if (menu) {
            // 应用菜单
            const appID = menu.appID;
            const menuID = menu.id;

            const app_menu = this.SidebarMenuRef.el.querySelector('a#menu-link-' + appID);
            app_menu.classList.add('active');
            const app_collapse = this.SidebarMenuRef.el.querySelector('div#menu-collapse-' + appID);
            if (!this.state.sidebarMinimize || !this.state.theme.sidebar_hover_maximize) {
                app_collapse.classList.add('show');

                // 子菜单
                const sub_menu = this.SidebarMenuRef.el.querySelector('a#menu-link-' + menuID);
                sub_menu.classList.add('active');
            } else {
                app_collapse.classList.remove('show');
            }

            return this.menuService.selectMenu(menu);
        }
    }

    getMenuItemIcon(level) {
        // 获取应用下的子菜单图标
        // 5级子菜单的图标，应该够使用了，不够请自行添加
        let icon = '';
        switch (level) {
            case 1:
                icon = 'fa fa-circle';
                break;
            case 2:
                icon = 'fa fa-dot-circle-o';
                break;
            case 3:
                icon = 'fa fa-circle-o';
                break;
            case 4:
                icon = 'fa fa-square';
                break;
            case 5:
                icon = 'fa fa-square-o';
                break;
            default:
                icon = 'fa fa-square-o';
        }
        return icon;
    }
}
SidebarMenu.template = 'web_theme.SidebarMenu';
SidebarMenu.components = {};
SidebarMenu.props = {};