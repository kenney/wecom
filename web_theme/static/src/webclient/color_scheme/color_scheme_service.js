/** @odoo-module **/

import { whenReady } from "@odoo/owl";
import { registry } from "@web/core/registry";
import { browser } from "@web/core/browser/browser";
import { cookie } from "@web/core/browser/cookie";

import { switchColorSchemeItem } from "./color_scheme_menu_items";

const serviceRegistry = registry.category("services");
const userMenuRegistry = registry.category("user_menuitems");

export class ColorSchemeService {
    constructor(env, { ui }) {
        this.ui = ui;
        whenReady(() => this.applyColorScheme());
    }

    /**
     * @returns {String} 最终用户配置的配色方案
     */
    get activeColorScheme() {
        return cookie.get("configured_color_scheme") || cookie.get("color_scheme") || "light";
    }

    /**
     * @returns {String} 应该从服务器加载的配色方案
     */
    get effectiveColorScheme() {
        return this.activeColorScheme;
    }

    /**
     * @param {String} scheme
     */
    switchToColorScheme(scheme) {
        cookie.set("configured_color_scheme", scheme);
        this.applyColorScheme();
    }
    /**
     * 检查当前加载的资源是否与当前有效的配色方案相对应。如果没有，请重新加载页面以获取正确的资产。
     */
    applyColorScheme() {
        const effectiveScheme = this.effectiveColorScheme;
        if (effectiveScheme !== (cookie.get("color_scheme") || "light")) {
            cookie.set("color_scheme", effectiveScheme);
            this.ui.block();
            this.reload();
        }
    }
    /**
     * 强制重新加载页面
     */
    reload() {
        browser.location.reload();
    }
}

export const colorSchemeService = {
    dependencies: ["ui"],

    start(env, services) {
        userMenuRegistry.add("color_scheme.switch", switchColorSchemeItem);
        return new ColorSchemeService(env, services);
    },
};
serviceRegistry.add("color_scheme", colorSchemeService);
