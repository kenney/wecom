/** @odoo-module **/

import { _lt, _t } from "@web/core/l10n/translation";
import { session } from "@web/session";
import { useService } from "@web/core/utils/hooks";
import { cookie as cookieManager } from "@web/core/browser/cookie";
import { browser } from "@web/core/browser/browser";

import {
    Component,
    onWillDestroy,
    useExternalListener,
    useEffect,
    onPatched,
    onRendered,
    useState,
    useRef,
    onWillUnmount,
} from "@odoo/owl";


export class ThemePanel extends Component {

    setup() {
        super.setup();

        this.user = useService("user");
        this.orm = useService("orm");
        this.notification = useService("notification");
        this.uiService = useService("ui");

        const color_cookie = cookieManager.get("color_scheme");
        this.ui = useService("ui");

        this.theme = session["theme"];
        const old_theme = this.theme;
        this.state = useState({
            theme: session["theme"],
            // old_theme: this.getDefaultTheme(this.theme),
            theme_has_changed: false,
            is_dark_mode: color_cookie === "dark" ? true : false,
        });


        // console.log("主题状态", this.state);
        onPatched(() => {
            // console.log("onPatched-----------theme", this.state.theme.main_submenu_position);
            // console.log("onPatched-----------old_theme", old_theme.main_submenu_position);
        });
        onRendered(() => {
            // console.log("onRendered-----------theme", this.state.theme.main_submenu_position);
            // console.log("onRendered-----------old_theme", old_theme.main_submenu_position);
            // if (JSON.stringify(this.state.theme) !== JSON.stringify(old_theme)) {
            //     this.state.theme_has_changed = true;
            // } else {
            //     this.state.theme_has_changed = false;
            // }
        });
    }

    /************************************************
     1.main
    ************************************************/
    // 1.1 在标签页中打开动作
    onToggleMainOpenActionInTabs(state) {
        this.state.theme.main_open_action_in_tabs = !state;
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "main_open_action_in_tabs": !state,
            },
        ])
    }

    // 1.2 子菜单位置
    onChangeSubmenuPosition(mode) {
        // console.log("子菜单位置1", mode.id, typeof (mode.id));;
        this.state.theme.main_submenu_position = mode.id;
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "main_submenu_position": mode.id.toString(),
            },
        ]);

    }


    /************************************************
     2.Layout
    ************************************************/
    onChangeLayoutMode(mode) {
        this.state.theme.menu_layout_mode = mode.id;
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "menu_layout_mode": mode.id.toString(),
            },
        ]);
    }

    /************************************************
     3.Color
    ************************************************/
    onClickThemeColor(color) {
        this.state.theme.theme_color = color.code;
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "theme_color": color.code,
            },
        ]);
    }

    /************************************************
     4.Sidebar
    ************************************************/
    onToggleSidebarDisplayNumberOfSubmenus(state) {
        this.state.theme.sidebar_display_number_of_submenus = !state
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "sidebar_display_number_of_submenus": !state,
            },
        ]);
    }
    onToggleSidebarShowMinimizeButton(state) {
        this.state.theme.sidebar_show_minimize_button = !state
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "sidebar_show_minimize_button": !state,
            },
        ]);
    }
    onToggleSidebarDefaultMinimizedn(state) {
        this.state.theme.sidebar_default_minimized = !state
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "sidebar_default_minimized": !state,
            },
        ]);
    }
    onToggleSidebarHoverMaximize(state) {
        this.state.theme.sidebar_hover_maximize = !state
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "sidebar_hover_maximize": !state,
            },
        ]);
    }

    /************************************************
     4.Header
    ************************************************/

    /************************************************
     5.Views
    ************************************************/
    onToggleDisplayScrollTopButton(state) {
        this.state.theme.display_scroll_top_button = !state;
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "display_scroll_top_button": !state
            },
        ]);
    }

    onToggleListHerderFixed(state) {
        this.state.theme.list_herder_fixed = !state;
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "list_herder_fixed": !state
            },
        ]);
    }

    onChangeListRowsLimit(ev) {
        this.state.theme.list_rows_limit = ev.target.value;
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "list_herder_fixed": ev.target.value
            },
        ]);
    }

    onChangeChatterPosition(position) {
        this.state.theme.form_chatter_position = position.id;
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "form_chatter_position": position.id.toString(),
            },
        ]);
    }

    /************************************************
     6.Footer
    ************************************************/
    onToggleDisplayFooter(state) {
        this.state.theme.display_footer = !state;
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "display_footer": !state
            },
        ]);
    }
    onToggleDisplayFooterDocument(state) {
        this.state.theme.display_footer_document = !state;
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "display_footer_document": !state
            },
        ]);
    }
    onToggleDisplayFooterSupport(state) {
        this.state.theme.display_footer_support = !state;
        this.orm.call("res.users", "set_user_theme", [
            session.uid,
            {
                "display_footer_support": !state
            },
        ]);
    }


    hasKey(key, obj) {
        if (obj.hasOwnProperty(key)) {
            return true;
        } else {
            return false;
        }
    }
}

ThemePanel.template = "web_theme.ThemePanel";
ThemePanel.components = {};
ThemePanel.props = {};