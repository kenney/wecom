/** @odoo-module **/

import { patch } from "@web/core/utils/patch";
import { session } from "@web/session";
import { SettingsPage } from "@web/webclient/settings_form_view/settings/settings_page";
import { onMounted, useEffect } from "@odoo/owl";

patch(SettingsPage.prototype, {
    setup() {
        super.setup(...arguments);
        const offset = 300;
        onMounted(() => {
            if (session["theme"]["display_scroll_top_button"]) {
                this.scrollTopButton = $(this.settingsRef.el).closest(".o_action_manager").find('.o_scroll_top')[0];
            }
        })

        const onClickScrollToTop = (ev) => {
            let scrollTopButton = ev.target;
            if (scrollTopButton.tagName == 'span') {
                scrollTopButton = scrollTopButton.parentNode;
            } else if (scrollTopButton.tagName == 'svg') {
                scrollTopButton = scrollTopButton.parentNode.parentNode;
            } else if (scrollTopButton.tagName == 'path') {
                scrollTopButton = scrollTopButton.parentNode.parentNode.parentNode;
            } else if (scrollTopButton.tagName == 'rect') {
                scrollTopButton = scrollTopButton.parentNode.parentNode.parentNode;
            }

            const duration = 500; //时间
            $(this.settingsRef.el).animate(
                {
                    scrollTop: 0
                },
                duration
            );
            return false;
        }

        const onScrollSettingsEl = (ev) => {
            if (ev.currentTarget.scrollTop > offset) {
                this.scrollTopButton.classList.remove('o_hidden');
                this.scrollTopButton.addEventListener("click", onClickScrollToTop);
            } else {
                this.scrollTopButton.classList.add('o_hidden');
                this.scrollTopButton.removeEventListener("click", onClickScrollToTop);
            }
        }

        useEffect((settingsEl) => {
            if (!session["theme"]["display_scroll_top_button"]) {
                return;
            }
            if (!settingsEl) {
                return;
            }

            settingsEl.addEventListener('scroll', onScrollSettingsEl);
        }, () => [this.settingsRef.el])
    },

})