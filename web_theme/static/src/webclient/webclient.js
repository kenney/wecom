/** @odoo-module **/

import { WebClient } from "@web/webclient/webclient";
import { useService } from "@web/core/utils/hooks";
import { WebThemeNavBar } from "./navbar/navbar";
import { session } from "@web/session";
import { sprintf } from "@web/core/utils/strings";

import { SidebarMenu } from './sidebar_menu/sidebar_menu';
import { WebThemeActionContainer } from './actions/action_container';
import { Component, onMounted, onWillStart, useExternalListener, useState } from "@odoo/owl";

export class WebThemeClient extends WebClient {
    setup() {
        super.setup();
        this.dm = useService("drawer_menu");
        this.title = useService("title");

        this.state = useState({
            ...this.state,
            theme: session["theme"]
        });

        // console.log("WebThemeClient setup", this.state);

        // 设置网站标题
        let system_name = session.system_name;
        const current_cid = session.user_companies.current_company; //当前公司id
        const display_company_name = session.display_company_name;
        if (display_company_name) {
            let allowed_companies = session.user_companies.allowed_companies;
            let current_company_name = getCurrentCompanyName(); //当前公司名称

            function getCurrentCompanyName() {
                for (var key in allowed_companies) {
                    let company = allowed_companies[key];
                    if (company.id === current_cid) {
                        return company.name;
                    }
                }
            }

            system_name = sprintf('%s - %s', current_company_name, system_name);
        }
        this.title.setParts({ zopenerp: system_name });

        // 主题
        this.state.theme.sidebarMinimize = false;
        if (this.state.theme.sidebar_default_minimized) {
            this.state.theme.sidebarMinimize = true;
        }

        onMounted(() => {
            // 定义挂载组件时应该执行的代码的钩子

            this.el = document.body;

            this.el.setAttribute('data-menu-mode', this.state.theme.menu_layout_mode);
            this.el.setAttribute('data-sidebar-default-minimized', this.state.theme.sidebar_default_minimized);
            this.el.setAttribute('data-sidebar-hover-maximize', this.state.theme.sidebar_hover_maximize);

            if (this.state.theme.sidebarMinimize) {
                this.el.setAttribute('data-sidebar-minimize', 'on');
            } else {
                this.el.setAttribute('data-sidebar-minimize', 'off');
            }
        });
    }
    _loadDefaultApp() {
        super._loadDefaultApp();
        if (this.state.theme.menu_layout_mode === 3) {
            return this.dm.toggle(true);
        }
    }
}
WebThemeClient.components = {
    ...WebClient.components,
    ActionContainer: WebThemeActionContainer,
    NavBar: WebThemeNavBar,
    SidebarMenu
};
WebThemeClient.template = 'web_theme.WebClient';
