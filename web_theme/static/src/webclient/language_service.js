/** @odoo-module **/

import { browser } from "@web/core/browser/browser";
import { registry } from "@web/core/registry";
import { session } from "@web/session";

export const languageService = {
    dependencies: ["user", "router"],
    start(env, { user, router }) {
        const availableLanguages = session.user_langs.langs;
        const currentLanguage = session.user_langs.current_lang;

        return {
            availableLanguages,
            get allLanguages() {
                return availableLanguages;
            },
            get currentLanguage() {
                return currentLanguage;
            },
        };
    },
};

registry.category("services").add("language", languageService);
