# -*- coding: utf-8 -*-

{
    "name": "Rainbow Theme",
    "author": "RStudio",
    "website": "https://eis-solution.coding.net/public/odoo/oec/git",
    "sequence": 0,
    "installable": True,
    "application": True,
    "auto_install": False,
    "category": "iERP Suites/Backend",
    "version": "17.0.0.1",
    "summary": "Back-end theme extension",
    "description": """
Web Client.
===========================

This module modifies the web plug-in to provide the following functions:
---------------------------
1) Responsiveness
2) Sidebars
3) Supports more than four levels of menus
4) Scroll to top button
5) Support custom browser window title name
""",
    "depends": [
        "web",
        "base_setup",
        "web_widgets",
    ],
    "excludes": [
        "web_enterprise",
    ],
    "data": [
        "security/ir.model.access.csv",
        "data/ir_config_parameter.xml",
        "data/res_theme_data.xml",
        "data/res_company_data.xml",
        "views/ir_module_views.xml",
        "views/webclient_templates.xml",
        # "views/res_company_views.xml",
        # "views/res_users_views.xml",
        # "views/res_groups_views.xml",
        # "views/calendar_views.xml",
        "views/res_config_settings_views.xml",
        "views/configurator_views.xml",
    ],
    "assets": {
        "web._assets_primary_variables": [
            (
                "after",
                "web/static/src/scss/primary_variables.scss",
                "web_theme/static/src/**/*.variables.scss",
            ),
            (
                "before",
                "web/static/src/scss/primary_variables.scss",
                "web_theme/static/src/scss/primary_variables.scss",
            ),
        ],
        "web._assets_secondary_variables": [
            (
                "before",
                "web/static/src/scss/secondary_variables.scss",
                "web_theme/static/src/scss/secondary_variables.scss",
            ),
        ],
        "web._assets_backend_helpers": [
            (
                "before",
                "web/static/src/scss/bootstrap_overridden.scss",
                "web_theme/static/src/scss/bootstrap_overridden.scss",
            ),
        ],
        "web.assets_frontend": [
            "web_theme/static/src/webclient/drawer_menu/drawer_menu_background.scss",  # 由登录页面使用
            "web_theme/static/src/webclient/navbar/navbar.scss",
            # 'web_theme/static/src/legacy/public/login.js',
        ],
        "web.assets_backend": [
            "web_theme/static/libs/bootstrap-icons/bootstrap-icons.css",
            "web_theme/static/src/webclient/**/*.scss",
            "web_theme/static/src/views/**/*.scss",
            "web_theme/static/src/core/**/*",
            "web_theme/static/src/webclient/**/*.js",
            "web_theme/static/src/webclient/**/*.xml",
            "web_theme/static/src/views/**/*.js",
            "web_theme/static/src/views/**/*.xml",
            # 不要在浅色模式下包含深色模式文件
            ("remove", "web_theme/static/src/**/*.dark.scss"),
        ],
        "web.assets_web": [
            ("replace", "web/static/src/main.js", "web_theme/static/src/main.js"),
        ],
        # ========= Dark Mode =========
        "web.dark_mode_variables": [
            # web._assets_primary_variables
            (
                "before",
                "web_theme/static/src/scss/primary_variables.scss",
                "web_theme/static/src/scss/primary_variables.dark.scss",
            ),
            (
                "before",
                "web_theme/static/src/**/*.variables.scss",
                "web_theme/static/src/**/*.variables.dark.scss",
            ),
            # web._assets_secondary_variables
            (
                "before",
                "web_theme/static/src/scss/secondary_variables.scss",
                "web_theme/static/src/scss/secondary_variables.dark.scss",
            ),
        ],
        "web.assets_web_dark": [
            ("include", "web.dark_mode_variables"),
            # web._assets_backend_helpers
            (
                "before",
                "web_theme/static/src/scss/bootstrap_overridden.scss",
                "web_theme/static/src/scss/bootstrap_overridden.dark.scss",
            ),
            (
                "after",
                "web/static/lib/bootstrap/scss/_functions.scss",
                "web_theme/static/src/scss/bs_functions_overridden.dark.scss",
            ),
            # assets_backend
            "web_theme/static/src/**/*.dark.scss",
        ],
    },
    "post_init_hook": "post_init_hook",  # 安装后执行的方法
    "uninstall_hook": "uninstall_hook",  # 卸载后执行的方法
    "license": "AGPL-3",
    "bootstrap": True,  # 加载登录屏幕的翻译，
}
