# -*- coding: utf-8 -*-

import json
from odoo import api, fields, models, tools, _
from odoo.addons.base.models.res_users import check_identity


class ResUsers(models.Model):
    _inherit = "res.users"

    lock_screen = fields.Boolean(string="Lock Screen", default=False)

    theme_id = fields.Many2one(
        "res.theme", string="Theme", store=True, domain="[('user_id', '=', id)]"
    )

    # ------------------------------------------------------------
    # 1.main
    # ------------------------------------------------------------
    main_open_action_in_tabs = fields.Boolean(
        related="theme_id.main_open_action_in_tabs", readonly=False
    )
    main_submenu_position = fields.Selection(
        related="theme_id.main_submenu_position",
        readonly=False,
    )

    # ------------------------------------------------------------
    # 2.layout
    # ------------------------------------------------------------
    menu_layout_mode = fields.Selection(
        related="theme_id.menu_layout_mode", readonly=False
    )

    # ------------------------------------------------------------
    # 6.Sidebar menu
    # ------------------------------------------------------------
    sidebar_display_number_of_submenus = fields.Boolean(
        related="theme_id.sidebar_display_number_of_submenus", readonly=False
    )
    sidebar_show_minimize_button = fields.Boolean(
        related="theme_id.sidebar_show_minimize_button", readonly=False
    )
    sidebar_default_minimized = fields.Boolean(
        related="theme_id.sidebar_default_minimized", readonly=False
    )
    sidebar_hover_maximize = fields.Boolean(
        related="theme_id.sidebar_hover_maximize", readonly=False
    )

    @api.model_create_multi
    def create(self, vals_list):
        """
        创建新用户时，创建主题
        """
        users = super(ResUsers, self).create(vals_list)
        for new_user in users:
            new_user.theme_id = (  # type: ignore
                self.env["res.theme"].sudo()._get_or_create_theme(new_user.id, "user")  # type: ignore
            )
        return users

    @api.model
    def set_user_theme(self, uid, theme):
        """
        设置用户主题
        :param uid: 用户id
        :param theme: 主题名称
        :return:
        """
        user = self.sudo().browse(uid)
        user.theme_id.sudo().write(theme)
