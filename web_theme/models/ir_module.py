# -*- coding: utf-8 -*-

from collections import OrderedDict
from odoo import api, fields, models, tools, _
from odoo.osv import expression
import logging

_logger = logging.getLogger(__name__)


class Module(models.Model):
    _inherit = "ir.module.module"

    hide = fields.Boolean("Hide", default=False)
