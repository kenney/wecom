/** @odoo-module */

import { patch } from "@web/core/utils/patch";
import { session } from "@web/session";
import { Chatter } from "@mail/core/web/chatter";
import { onMounted, useEffect } from "@odoo/owl";

patch(Chatter.prototype, {
    setup() {
        super.setup();
        const self = this;
        const offset = 300;

        const onClickScrollToTop = (ev) => {
            let scrollTopButton = ev.target;
            if (scrollTopButton.tagName == 'span') {
                scrollTopButton = scrollTopButton.parentNode;
            } else if (scrollTopButton.tagName == 'svg') {
                scrollTopButton = scrollTopButton.parentNode.parentNode;
            } else if (scrollTopButton.tagName == 'path') {
                scrollTopButton = scrollTopButton.parentNode.parentNode.parentNode;
            } else if (scrollTopButton.tagName == 'rect') {
                scrollTopButton = scrollTopButton.parentNode.parentNode.parentNode;
            }

            const duration = 500; //时间
            $(this.rootRef.el).animate(
                {
                    scrollTop: 0
                },
                duration
            );
            return false;
        }

        const onScrollChatterEl = (ev) => {
            if (ev.currentTarget.scrollTop > offset) {
                this.scrollTopButton.classList.remove('o_hidden');
                this.scrollTopButton.addEventListener("click", onClickScrollToTop);
            } else {
                this.scrollTopButton.classList.add('o_hidden');
                this.scrollTopButton.removeEventListener("click", onClickScrollToTop);
            }
        }

        useEffect((ChatterEl) => {
            if (session["theme"]["display_scroll_top_button"]) {
                self.scrollTopButton = $(ChatterEl).closest(".o_action_manager").find('.o_scroll_top')[0];
            }
            if (!session["theme"]["display_scroll_top_button"]) {
                return;
            }
            if (!ChatterEl) {
                return;
            }
            ChatterEl.addEventListener('scroll', onScrollChatterEl);
        }, () => [this.rootRef.el])
    },
})