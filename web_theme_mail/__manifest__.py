# -*- coding: utf-8 -*-

{
    "name": "Rainbow Mail Theme",
    "author": "RStudio",
    "website": "https://eis-solution.coding.net/public/odoo/oec/git",
    "sequence": 0,
    "installable": True,
    "application": False,
    "auto_install": False,
    "category": "iERP Suites/Backend",
    "version": "17.0.0.1",
    "summary": "",
    "description": """""",
    "depends": ["web_theme", "mail"],
    "excludes": [],
    "data": [],
    "assets": {
        "web._assets_primary_variables": [],
        "web._assets_secondary_variables": [],
        "web._assets_backend_helpers": [],
        "web.assets_frontend": [],
        "web.assets_backend": [
            "web_theme_mail/static/src/core/**/*",
        ],
    },
    "license": "AGPL-3",
}
