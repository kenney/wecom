# -*- coding: utf-8 -*-


from . import res_company
from . import res_config_settings
from . import res_users


from . import wecom_apps
from . import wecom_contacts_block
from . import wecom_user
from . import wecom_department
from . import wecom_tag
