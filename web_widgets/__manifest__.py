# -*- coding: utf-8 -*-

{
    "name": "iErp Web Widgets",
    "author": "RStudio",
    "category": "iErp Suites/Widget",
    "summary": "iErp Widgets",
    "website": "https://eis-solution.coding.net/public/odoo/oec/git",
    "version": "17.0.0.1",
    "description": """

""",
    "depends": [
        "web",
        "attachment_indexation",
    ],
    "external_dependencies": {
        "python": [
            # "pyzbar",
        ],
    },
    "data": [],
    "assets": {
        # "web.assets_common": [
        #     "web_widgets/static/fonts/fonts.scss",
        # ],
        "web.assets_backend": [
            "web_widgets/static/fonts/fonts.scss",
            "web_widgets/static/src/views/**/*.js",
            "web_widgets/static/src/views/**/*.xml",
            "web_widgets/static/src/views/**/*.scss",
            "web_widgets/static/src/webclient/**/*",
        ],
        "web.assets_frontend": [
            # "web_widgets/static/lib/officetohtml/**/*",
        ],
    },
    "sequence": 500,
    "installable": True,
    "auto_install": True,
    "application": False,
    "license": "AGPL-3",
}
