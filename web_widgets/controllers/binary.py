# -*- coding: utf-8 -*-

import os
import logging

from odoo import SUPERUSER_ID, _, http
from odoo.exceptions import AccessError, MissingError, ValidationError
from odoo.fields import Command
from odoo.http import request

from odoo.addons.web.controllers.binary import Binary
from odoo.exceptions import AccessError, UserError
from odoo.tools import file_open, file_path, replace_exceptions


_logger = logging.getLogger(__name__)


class WebWidgetBinary(Binary):

    @http.route(
        ["/<string:model>/<string:field>/<int:id>.<string:filetype>"],
        type="http",
        # auth="public",
        auth="user",
    )
    def get_office_url(
        self,
        xmlid=None,
        model="ir.attachment",
        id=None,
        field="raw",
        filetype=None,
        filename=None,
        filename_field="name",
        mimetype=None,
        unique=False,
        download=False,
        access_token=None,
        nocache=False,
    ):
        print(model, field, id, filename, filename_field, filetype)

        with replace_exceptions(UserError, by=request.not_found()):
            record = request.env["ir.binary"]._find_record(
                xmlid, model, id and int(id), access_token
            )
            stream = request.env["ir.binary"]._get_stream_from(
                record, field, filename, filename_field, mimetype
            )
        send_file_kwargs = {"as_attachment": False}
        if unique:
            send_file_kwargs["immutable"] = True
            send_file_kwargs["max_age"] = http.STATIC_CACHE_LONG
        if nocache:
            send_file_kwargs["max_age"] = None

        res = stream.get_response(**send_file_kwargs)
        res.headers["Content-Security-Policy"] = "default-src 'none'"
        return "https://wecom.rstudio.xyz/web_widgets/static/files/office/sale_order_outsourcing/finalized_office_file_4.docx"
        return res
