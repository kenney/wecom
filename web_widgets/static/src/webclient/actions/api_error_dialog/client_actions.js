/** @odoo-module **/

import { registry } from "@web/core/registry";
import { ApiErrorDialog } from "./api_error_dialog";



export function apiDialogAction(env, action) {
    const params = action.params || {};
    env.services.dialog.add(ApiErrorDialog, params, {});
}
registry.category("actions").add("api_error_dialog", apiDialogAction);