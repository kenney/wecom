/** @odoo-module **/

import { Dialog } from "@web/core/dialog/dialog";
import { _lt } from "@web/core/l10n/translation";
import { markup } from "@odoo/owl";
const { Component, } = owl;


export class ApiErrorDialog extends Component {
    setup() {
        this.title = this.props.title;
        this.size = this.props.size;
        this.code = this.props.code;
        this.description = this.props.description;
        this.method = markup(this.props.method);
        this.details = this.props.details;

        this.bodyClass = "bg-warning";
        this.contentClass = "bg-warning";
    }
}
ApiErrorDialog.components = {
    Dialog
};
ApiErrorDialog.template = "web.ApiErrorDialog";
ApiErrorDialog.title = _lt("API Error");