/** @odoo-module **/


import { registry } from '@web/core/registry';

export function openListAction(env, action) {

	if (Object.keys(action).length > 0) {
		window.dispatchEvent(
			new KeyboardEvent('keydown', {
				key: 'b',
				ctrlKey: false,
				shiftKey: false,
				altKey: true,
				metaKey: false,
				bubbles: true,
				cancelable: true
			})
		);
	} else {
		return;
	}
}

registry.category('actions').add('open_list_action', openListAction);
