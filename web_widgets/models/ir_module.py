# -*- coding: utf-8 -*-

import os
import platform
import base64
import pathlib
from datetime import datetime, timedelta

from odoo import api, fields, models, tools, _
from odoo.tools.misc import file_path
import logging

_logger = logging.getLogger(__name__)


class Module(models.Model):
    _inherit = "ir.module.module"

    @api.model
    def get_module_installation_status(self, addon_name=None):
        """
        获取 addon 是否安装
        """
        addon = self.sudo().search([("name", "=", addon_name)])
        if not addon:
            # return {
            #     "exist": False,
            #     "moduleId": 0,
            # }
            return False
        else:
            # addon.button_install()
            # return {
            #     "exist": True,
            #     "moduleId": addon.id,
            # }
            return True
