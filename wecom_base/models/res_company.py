# -*- coding: utf-8 -*-


import io
import logging
import os

from odoo import api, fields, models, tools, _


class Company(models.Model):
    _inherit = "res.company"

    # 基础
    short_name = fields.Char("Short Name", translate=True)
    is_wecom_organization = fields.Boolean("WeCom organization", default=False)
    corpid = fields.Char("Corp ID")

    # 企业的jsapi_ticket
    corp_jsapi_ticket = fields.Char(string="Corp JSAPI Ticket", copy=False)
    corp_jsapi_ticket_expiration_time = fields.Datetime(
        string="Expiration time of Corp JSAPI ticket", copy=False
    )
