/** @odoo-module **/

import { _t } from "@web/core/l10n/translation";
import { sprintf } from "@web/core/utils/strings";
import { registry } from "@web/core/registry";
import { useService } from "@web/core/utils/hooks";
import { url } from "@web/core/utils/urls";
import { standardFieldProps } from "@web/views/fields/standard_field_props";
import { FileUploader } from "@web/views/fields/file_handler";

import { Component, onWillStart, onWillUpdateProps, useState } from "@odoo/owl";

export class MsOfficeViewerField extends Component {
    static template = "web.MsOfficeViewerField";
    static components = {
        FileUploader,
    };
    static props = {
        ...standardFieldProps,
    };

    setup() {
        this.notification = useService("notification");
        this.rpc = useService("rpc");
        this.orm = useService("orm");
        this.state = useState({
            isValid: true,
            objectUrl: "",
            officeUrl: "",
        });
        onWillStart(async () => {
            if (this.props.record.data[this.props.name]) {
                const model_name = this.props.record.resModel;
                const field_name = this.props.name;
                const res_id = this.props.record.resId;
                const office_url = await this.orm.call('ir.module.module', 'get_office_url', [
                    model_name,
                    field_name,
                    res_id,
                ]);

                this.state.officeUrl = office_url;
            }
        });
        onWillUpdateProps((nextProps) => {
            if (nextProps.readonly) {
                this.state.objectUrl = "";
            }
        });
    }

    get office_url() {
        if (!this.state.isValid || !this.props.record.data[this.props.name]) {
            return null;
        }
        let office_url = ""
        if (this.state.officeUrl !== "") {
            office_url = this.state.officeUrl
        }

        return `https://view.officeapps.live.com/op/view.aspx?src=${office_url}`;
    }

    update({ data }) {
        const changes = { [this.props.name]: data || false };
        return this.props.record.update(changes);
    }

    onFileRemove() {
        this.state.isValid = true;
        this.update({});
    }

    async onFileUploaded(file) {
        const file_data = {
            name: file.name,
            type: file.type,
            data: file.data,
            objectUrl: file.objectUrl,
            size: file.size,
        };

        const model_name = this.props.record.resModel;
        const field_name = this.props.name;
        const res_id = this.props.record.resId;
        const result = await this.orm.call('ir.module.module', 'upload_office_file', [
            file_data.name,
            model_name,
            field_name,
            res_id,
            file_data.data
        ]);

        if (result['state']) {
            const data = file_data.data;
            this.state.isValid = true;
            this.state.officeUrl = result['url'];
            this.state.objectUrl = file_data.objectUrl;
            this.notification.add(
                result['msg'],
                {
                    title: _t("Successfully uploaded the file!"),
                    type: "success",
                    sticky: false,
                });

            this.update({ data });
        } else {
            this.notification.add(result['msg'], {
                title: _t("Failed to upload file!"),
                type: "warning",
                sticky: false,
            });
        }
    }

    onLoadFailed() {
        this.state.isValid = false;
        this.notification.add(_t("Unable to display the selected office file"), {
            type: "danger",
        });
    }
}

export const msOfficeViewerField = {
    component: MsOfficeViewerField,
    displayName: _t("MS Office Viewer"),
    supportedOptions: [
        {
            label: _t("Preview image"),
            name: "preview_image",
            type: "field",
            availableTypes: ["binary"],
        },
    ],
    supportedTypes: ["binary"],
};

registry.category("fields").add("ms_office_viewer", msOfficeViewerField);
