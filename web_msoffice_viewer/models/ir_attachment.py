# -*- coding: utf-8 -*-

import os
import platform
import base64
import pathlib
from datetime import datetime, timedelta

from odoo import api, fields, models, tools, _
from odoo.tools.misc import file_path
import logging

_logger = logging.getLogger(__name__)


class IrAttachment(models.Model):
    _inherit = "ir.attachment"

    # mimetype = fields.Char('Mime Type', readonly=True)

    @api.model
    def get_attachment_mimetype(self, model_name, field_name, res_id):
        """
        获取附件的 mime 类型
        """
        res = self.search_read(
            domain=[
                ("res_name", "=", model_name),
                ("res_field", "=", field_name),
                ("res_id", "=", res_id),
            ],
            fields=["mimetype"],
        )
        return res[0]["mimetype"]
