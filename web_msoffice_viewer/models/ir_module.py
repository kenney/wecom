# -*- coding: utf-8 -*-

import os
import platform
import base64
import pathlib
from datetime import datetime, timedelta

from odoo import api, fields, models, tools, _
from odoo.tools.misc import file_path
import logging

_logger = logging.getLogger(__name__)


class Module(models.Model):
    _inherit = "ir.module.module"

    @api.model
    def upload_office_file(
        self, file_name, module_name, field_name, res_id, data_base64_str
    ):
        """
        上传office 文档到模块下的静态目录
        """
        file_format = file_name.split(".")[1]

        storage_dir = ""
        if "." in module_name:
            storage_dir = module_name.replace(".", "_")

        app_path = pathlib.Path(__file__).resolve().parent.parent
        office_file_store_path = f"{app_path}/static/files/office/{storage_dir}"

        office_file_name = f"{field_name}_{str(res_id)}.{file_format}"
        office_file_path = f"{office_file_store_path}/{office_file_name}"

        result = {
            "state": False,
            "msg": "",
            "url": "",
        }
        try:
            if not os.path.exists(office_file_store_path):
                os.makedirs(office_file_store_path)

            with open(office_file_path, "wb") as f:
                f.write(base64.b64decode(data_base64_str))
        except Exception as e:
            result = {
                "state": False,
                "file_name": "",
                "url": "",
                "msg": _("Failed to upload file %s, reason:%s" % (file_name, str(e))),
            }
        else:
            base_url = self.env["ir.config_parameter"].sudo().get_param("web.base.url")
            url = ""
            if base_url[-1] == "/":
                url = "%s%s" % (
                    base_url,
                    f"web_msoffice_viewer/static/files/office/{storage_dir}/{office_file_name}",
                )
            else:
                url = "%s/%s" % (
                    base_url,
                    f"web_msoffice_viewer/static/files/office/{storage_dir}/{office_file_name}",
                )

            result = {
                "state": True,
                "file_name": file_name,
                "new_file_name": office_file_name,
                "url": url,
                "msg": _("Successfully uploaded %s" % file_name),
            }

        return result

    @api.model
    def get_office_url(self, module_name, field_name, res_id):
        """
        获取office文件的URL
        """
        storage_dir = ""
        if "." in module_name:
            storage_dir = module_name.replace(".", "_")
        app_path = pathlib.Path(__file__).resolve().parent.parent
        office_file_store_path = f"{app_path}/static/files/office/{storage_dir}"
        office_file_name = f"{field_name}_{str(res_id)}"
        matched_office_file = ""

        if not os.path.exists(office_file_store_path):
            os.makedirs(office_file_store_path)

        files = os.listdir(office_file_store_path)
        for file in files:
            file_path = os.path.join(office_file_store_path, file)
            if os.path.isfile(file_path):
                file_name = file.split(".")[0]
                if file_name == office_file_name:
                    matched_office_file = file

        base_url = self.env["ir.config_parameter"].sudo().get_param("web.base.url")
        url = ""
        if base_url[-1] == "/":
            url = "%s%s" % (
                base_url,
                f"web_msoffice_viewer/static/files/office/{storage_dir}/{matched_office_file}",
            )
        else:
            url = "%s/%s" % (
                base_url,
                f"web_msoffice_viewer/static/files/office/{storage_dir}/{matched_office_file}",
            )

        return url
