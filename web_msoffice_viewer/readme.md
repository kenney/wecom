# 微软Office文件在线预览

## 适用场景

1. 可以对方公示的office文档
2. 对office文档是否私密无感

## 微软Office文件在线预览使用说明

1. 在模型中定义一个二进制的字段
```python
office_file = fields.Binary(string="office file", attachment=True, copy=False)
```

2. 在视图中如下使用：
```xml
<field name="office_file" widget="ms_office_viewer"/>
```
2. 使用微软提供的在线预览功能，此功能要求必须在互联网上可以访问您的文件，故此功能widget同时将office文件保存到目录`web_widgets/static/files/office/{模型名称}`

3. 在文件`web_widgets/static/src/views/fields/ms_office_viewer/ms_office_viewer_filed.xml` 中 `acceptedFileExtensions` 定义了允许上传的文件格式，有额外需求可以自己修改需求的

4. accept的接受的微软office文档 Mime 类型列表：

| 类型/子类型 | 扩展名 |  说明 |
|----------|:-------------|------:|
|application/msword	|doc | MS Word Document |
|application/vnd.openxmlformats-officedocument.wordprocessingml.document |docx | MS Word Document |
|application/msword	|dot | MS Word Template |
|application/vnd.ms-excel|	xla| |
|application/vnd.ms-excel|	xlc| MS Excel Chart|
|application/vnd.ms-excel|	xlm| MS Excel Macro|
|application/vnd.ms-excel|	xls| MS Excel Spreadsheet|
|application/vnd.openxmlformats-officedocument.spreadsheetml.sheet|	xlsx| MS Excel Spreadsheet|
|application/vnd.ms-excel|	xlt| |
|application/vnd.ms-excel|	xlw| MS Excel Workspace|
|application/vnd.ms-outlook|	msg| |
|application/vnd.ms-pkicertstore|	sst| |
|application/vnd.ms-pkiseccat|	cat| |
|application/vnd.ms-pkistl|	stl| |
|application/vnd.ms-powerpoint|	pot| MS PowerPoint Template|
|application/vnd.ms-powerpoint|	pps| MS PowerPoint Slideshow|
|application/vnd.ms-powerpoint|	ppt| MS PowerPoint Presentation|
|application/vnd.openxmlformats-officedocument.presentationml.presentation|	pptx| MS PowerPoint Presentation|
|application/vnd.ms-project|	mpp| MS Project Project|
|application/vnd.ms-works|	wcm| |
|application/vnd.ms-works|	wdb| MS Works Database|
|application/vnd.ms-works|	wks| |
|application/vnd.ms-works|	wps| Works Text Document|

