# -*- coding: utf-8 -*-

{
    "name": "MS Office Viewer",
    "author": "RStudio",
    "category": "iERP Suites/Widget",
    "summary": "iERP Widgets",
    "website": "https://eis-solution.coding.net/public/odoo/oec/git",
    "version": "17.0.0.1",
    "description": """

""",
    "depends": [
        "web",
        "attachment_indexation",
    ],
    "external_dependencies": {
        "python": [],
    },
    "data": [],
    "assets": {
        "web.assets_backend": [
            "web_msoffice_viewer/static/src/views/**/*.js",
            "web_msoffice_viewer/static/src/views/**/*.xml",
            "web_msoffice_viewer/static/src/views/**/*.scss",
        ],
        "web.assets_frontend": [
            # "web_widgets/static/lib/officetohtml/**/*",
        ],
    },
    "sequence": 500,
    "installable": True,
    "auto_install": True,
    "application": False,
    "license": "AGPL-3",
}
