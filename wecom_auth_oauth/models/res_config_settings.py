# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

from odoo.addons.wecom_api.api.wecom_abstract_api import ApiException  # type: ignore


import werkzeug.urls
import werkzeug.utils
import urllib
import datetime
import logging

_logger = logging.getLogger(__name__)


class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    company_id = fields.Many2one(
        "res.company",
        string="Company",
        required=True,
        default=lambda self: self.env.company,
    )

    auth_app_id = fields.Many2one(
        related="company_id.auth_app_id",
        readonly=False,
    )
    auth_agentid = fields.Integer(related="auth_app_id.agentid", readonly=False)
    auth_secret = fields.Char(related="auth_app_id.secret", readonly=False)

    auth_app_jsapi_ticket = fields.Char(
        related="auth_app_id.jsapi_ticket", readonly=False
    )
    auth_app_jsapi_ticket_expiration_time = fields.Datetime(
        related="auth_app_id.jsapi_ticket_expiration_time", readonly=False
    )

    auth_app_config_ids = fields.One2many(
        # related="company_id.contacts_auto_sync_hr_enabled", readonly=False
        related="auth_app_id.app_config_ids",
        # domain="[('company_id', '=', company_id),('app_id', '=', contacts_app_id)]",
        readonly=False,
    )

    def enable_wecom_quick_login(self):
        ir_model_data = self.env["ir.model.data"]
        provider_wecom_quick_login = ir_model_data._xmlid_lookup(
            "wecom_auth_oauth.provider_wecom_quick_login",
        )[1]
        providers = [provider_wecom_quick_login]
        for provider in providers:
            p = self.env["auth.oauth.provider"].search([("id", "=", provider)], limit=1)
            if p:
                p.enabled = True

    def set_oauth_provider_wecom(self):
        self.auth_app_id.set_oauth_provider_wecom()  # type: ignore

    def generate_parameters(self):
        """
        生成参数
        :return:
        """
        code = self.env.context.get("code")
        if bool(code) and code == "auth":
            for record in self:
                # if not record.contacts_app_id:
                #     raise ValidationError(_("Please bind contact app!"))
                # else:
                record.auth_app_id.with_context(code=code).generate_parameters()  # type: ignore
        super(ResConfigSettings, self).generate_parameters()  # type: ignore

    def get_app_info(self):
        """
        获取应用信息
        :return:
        """
        app = self.env.context.get("app")
        for record in self:
            if app == "auth" and (
                record.auth_app_id.agentid == 0 or record.auth_app_id.secret == ""  # type: ignore
            ):
                raise UserError(_("Auth application ID and secret cannot be empty!"))
            else:
                record.auth_app_id.get_app_info()  # type: ignore
        super(ResConfigSettings, self).get_app_info()  # type: ignore

    def get_auth_app_jsapi_ticket(self):
        """
        获取身份认证应用的jsapi_ticket
        :return:
        """
        ir_config = self.env["ir.config_parameter"].sudo()
        debug = ir_config.get_param("wecom.debug_enabled")
        if debug:
            _logger.info(
                _("Start getting ticket for company [%s]") % (self.company_id.name)  # type: ignore
            )
        try:
            if (
                self.auth_app_jsapi_ticket_expiration_time
                and self.auth_app_jsapi_ticket_expiration_time > datetime.now()
            ):
                # 未过期，
                # print("未过期")
                msg = {
                    "title": _("Tips"),
                    "message": _("Ticket is still valid, and no update is required!"),
                    "sticky": False,
                }
                return self.env["wecomapi.tools.action"].WecomInfoNotification(msg)
            else:
                # print("过期")
                wecom_api = self.env["wecom.service_api"].InitServiceApi(self.company_id.corpid, self.auth_app_id.secret)  # type: ignore
                response = wecom_api.httpCall(
                    self.env["wecom.service_api_list"].get_server_api_call(
                        "AGENT_GET_TICKET"
                    ),
                    {"type": "agent_config"},
                )
                # print(response)
        except ApiException as ex:
            return self.env["wecomapi.tools.action"].ApiExceptionDialog(
                ex, raise_exception=True
            )
        else:
            if response["errcode"] == 0:
                self.write(
                    {
                        "auth_app_jsapi_ticket": response["ticket"],
                        "auth_app_jsapi_ticket_expiration_time": datetime.now()
                        + timedelta(seconds=response["expires_in"]),
                    }
                )
                msg = {
                    "title": _("Tips"),
                    "message": _("Successfully obtained ticket!"),
                    "sticky": False,
                    "next": {
                        "type": "ir.actions.client",
                        "tag": "reload",
                    },
                }
                return self.env["wecomapi.tools.action"].WecomSuccessNotification(msg)
