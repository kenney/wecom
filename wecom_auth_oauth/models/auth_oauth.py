# -*- coding: utf-8 -*-

from odoo import fields, models


class AuthOAuthProvider(models.Model):
    _inherit = "auth.oauth.provider"

    is_woecom = fields.Boolean(string="WeCom OAuth", default=False)
    description = fields.Html(string="Description", translate=True)
