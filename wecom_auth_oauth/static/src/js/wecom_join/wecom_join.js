/** @odoo-module **//** @odoo-module **/

import publicWidget from "@web/legacy/js/public/public_widget";
// import * as ww from '@wecom/jssdk'

publicWidget.registry.WecomJion = publicWidget.Widget.extend({
    selector: 'form.oe_login_form',

    init() {
        this._super(...arguments);
        this.rpc = this.bindService("rpc");
    },
    start: function () {
        this.companies = this.rpc({
            route: "/wecom_login_info",
            params: {

            },
        });
    },
});
