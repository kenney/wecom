/** @odoo-module **/

import publicWidget from "@web/legacy/js/public/public_widget";
// import * as ww from '@wecom/jssdk'

publicWidget.registry.WecomLogin = publicWidget.Widget.extend({
    selector: '.o_login_auth',

    events: {
        'click a': '_onClick',
    },
    jsLibs: [
        document.location.protocol + "//wwcdn.weixin.qq.com/node/open/js/wecom-jssdk-1.3.1.js",
    ],
    _onClick: async function (ev) {
        ev.preventDefault(); //阻止默认行为
        let auth_url = $(ev.currentTarget).attr('href');
        console.log(auth_url);
    },
});
