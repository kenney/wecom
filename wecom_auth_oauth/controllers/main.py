# -*- coding: utf-8 -*-

# !参考 \addons\auth_oauth\controllers\main.py

import base64
import functools
import json
import logging
import os

import werkzeug.urls
import werkzeug.utils
from werkzeug.exceptions import BadRequest

from odoo import api, http, SUPERUSER_ID, _
from odoo.exceptions import AccessDenied
from odoo.http import request, Response
from odoo import registry as registry_get
from odoo.tools.misc import clean_context

from odoo.addons.auth_signup.controllers.main import AuthSignupHome as Home
from odoo.addons.web.controllers.utils import ensure_db, _get_login_redirect_url


_logger = logging.getLogger(__name__)

WECOM_BROWSER_MESSAGES = {
    "not_wecom_browser": _(
        "The current browser is not an WeCom built-in browser, so the one-click login function cannot be used."
    ),
    "is_wecom_browser": _(
        "It is detected that the page is opened in the built-in browser of WeCom, please select company."
    ),
}


class WeComOAuthLogin(Home):
    def list_providers(self):
        providers = super(WeComOAuthLogin, self).list_providers()

        for provider in providers:
            if (
                "login.work.weixin.qq.com/wwlogin/sso/login"
                in provider["auth_endpoint"]
            ):
                # 链接格式
                # https://login.work.weixin.qq.com/wwlogin/sso/login?login_type=LOGIN_TYPE&appid=APPID&redirect_uri=REDIRECT_URI&state=STATE
                return_url = request.httprequest.url_root + "wecom/authorize"
                state = self.get_state(provider)
                params = dict(
                    appid=False,
                    redirect_uri=return_url,
                    response_type="code",
                    scope=provider["scope"],
                    state=json.dumps(state),
                )
                provider["auth_link"] = "%s?%s%s" % (
                    provider["auth_endpoint"],
                    werkzeug.urls.url_encode(params),  # type: ignore
                    "#wechat_redirect",
                )

        return providers


class OAuthController(http.Controller):
    @http.route("/wwcom_login_info", type="json", auth="none")
    def wecom_get_login_info(self, **kwargs):
        data = {}
        if "is_wecom_browser" in kwargs:
            if kwargs["is_wecom_browser"]:
                data = {
                    "is_wecom_browser": True,
                    "msg": WECOM_BROWSER_MESSAGES["is_wecom_browser"],
                    "companies": [],
                }
            else:
                data = {
                    "is_wecom_browser": False,
                    "msg": WECOM_BROWSER_MESSAGES["not_wecom_browser"],
                    "companies": [],
                }
        else:
            data = {
                "join_button_name": _("Join WeCom,Become our employee."),
                "companies": [],
            }

        # 获取 标记为 企业微信组织 的公司
        companies = request.env["res.company"].search(  # type: ignore
            [(("is_wecom_organization", "=", True))]
        )

        if len(companies) > 0:
            for company in companies:
                # app_config = request.env["wecom.app_config"].sudo()
                # contacts_app = company.contacts_app_id.sudo()  # 通讯录应用
                auth_app = company.auth_app_id.sudo()  # 验证登录应用
                data["companies"].append(
                    {
                        "id": company["id"],
                        "name": company["abbreviated_name"],
                        "fullname": company["name"],
                        "appid": company["corpid"],
                        "agentid": auth_app.agentid if auth_app else 0,
                        "enabled_join": company["wecom_contacts_join_qrcode_enabled"],
                        "join_qrcode": company["wecom_contacts_join_qrcode"],
                    }
                )
        return data
