/** @odoo-module */

import { registry } from '@web/core/registry';
import { _t } from "@web/core/l10n/translation";
import { Component } from "@odoo/owl";

export class ResConfigWecomNavigation extends Component {
    static template = "res_config_wecom_navigation";

    setup() {

    }

    onClickJumpAnchor() {

    }
}

export const resConfigWecomNavigation = {
    component: ResConfigWecomNavigation,
};
resConfigWecomNavigation.template = 'res_config_wecom_navigation';
resConfigWecomNavigation.props = {};
registry.category('view_widgets').add('res_config_wecom_navigation', resConfigWecomNavigation);