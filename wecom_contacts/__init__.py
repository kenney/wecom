# -*- coding: utf-8 -*-


from . import models


def post_init_hook(env):
    # env["ir.http"].clear_caches()
    # env["ir.ui.view"].clear_caches()
    env["ir.http"]._invalidate_cache()
    env["ir.ui.view"]._invalidate_cache()
